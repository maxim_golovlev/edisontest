//
//  FeedControllerPresenter.swift
//  EdisonTest
//
//  Created by Admin on 10.11.17.
//  Copyright (c) 2017 Admin. All rights reserved.
//

import UIKit
import MapKit

protocol FeedControllerPresenterProtocol: class {
  weak var view:FeedControllerViewProtocol? { get set }
    func fetchCrimes(date: Date, location: CLLocationCoordinate2D, area: Double)
    func fetchCrimesCategories(date: Date)
}

class FeedControllerPresenter {
  
  // MARK: - Public variables
  weak var view:FeedControllerViewProtocol?
  
  // MARK: - Private variables
  
  // MARK: - Initialization
  init(view:FeedControllerViewProtocol) {
    self.view = view
  }
}

extension FeedControllerPresenter: FeedControllerPresenterProtocol {
  
    func fetchCrimesCategories(date: Date) {
        
        self.view?.startLoading()
        
        CrimesManager.shared.fetchCrimeCategories(date: date)
            .then { (categoryIds) -> Void in
                self.view?.categoriesUploaded(categoryIds: categoryIds)
            }
            .always {
                self.view?.stopLoading()
            }
            .catch { (error) in
                if case let ResponseError.withMessage(msg) = error {
                    self.view?.showAlert(title: nil, message: msg)
                }
        }
    }
    
    func fetchCrimes(date: Date, location: CLLocationCoordinate2D, area: Double) {
        
        self.view?.startLoading()
        
        CrimesManager.shared.fetchCrimes(date: date, location: location, area: area)
            .then { (crimesIds) -> Void in
                self.view?.crimesUploaded(crimeIds: crimesIds)
            }
            .always {
                self.view?.stopLoading()
            }
            .catch { (error) in
                if case let ResponseError.withMessage(msg) = error {
                    self.view?.showAlert(title: nil, message: msg)
                }
        }
    }
}
