//
//  FeedControllerViewController.swift
//  EdisonTest
//
//  Created by Admin on 10.11.17.
//  Copyright (c) 2017 Admin. All rights reserved.
//

import UIKit
import MapKit
import TableKit
import ActionSheetPicker_3_0

protocol FeedControllerViewProtocol: BaseView {
    func crimesUploaded(crimeIds: [Int32])
    func categoriesUploaded(categoryIds: [String])
}

class FeedViewController: UIViewController {
  
  // MARK: - Public properties
  
   lazy var presenter:FeedControllerPresenterProtocol = FeedControllerPresenter(view: self)
  
  // MARK: - Private properties
  
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableDirector = TableDirector.init(tableView: tableView)
            tableView.tableFooterView = UIView()
        }
    }
    @IBOutlet weak var mapkitView: MKMapView!
    private var tableDirector: TableDirector!
    private var crimeCategories = [String]()
    private var originalCrimes = [Crime]()
    private var filteredCrimes = [Crime]()
    private var selectedCategory: String?
    private var selectedDate: Date = Date()
    private var selectedLocation = londonLocation
    private static let londonLocation = CLLocationCoordinate2D.init(latitude: 51.509865, longitude: -0.118092)
    private static let defaultDateKey = "Date: \(Date().toString(format: "MMM d, yyyy"))"
    private static let defaultCategoryKey = "Category: All crimes"
    private let searchArea = 0.003
    // MARK: - View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    ifLauchedBefore {
         showAlert(title: "", message: "To change the location please perform a long press touch on map")
    }
    
    renderOptionsSection()
    refreshMap()
  }
  
  // MARK: - Display logic
  
  // MARK: - Actions
    @IBAction func openSavedCrimes(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier :"DetailViewController") as? DetailViewController {
            vc.crimes = Crime.fetch(context: CrimesManager.currentContext) ?? []
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func createNewLocation(_ sender: Any) {
        guard let longPress = sender as? UILongPressGestureRecognizer else { return }
        let touchLocation = longPress.location(in: mapkitView)
        let coordinate = mapkitView.convert(touchLocation, toCoordinateFrom: mapkitView)
        mapkitView.removeOverlays(mapkitView.overlays)
        let circle = MKCircle(center: coordinate, radius: 5)
        mapkitView.add(circle)
        
        selectedLocation = coordinate
    }
    
    @IBAction func refreshMap(_ sender: UIBarButtonItem) {
        refreshMap()
    }
    @IBAction func search(_ sender: UIBarButtonItem) {
        attemptSearching(searchArea: searchArea)
    }
    
    // MARK: - Overrides
    
  // MARK: - Private functions
    
    func ifLauchedBefore(completion: () -> ()) {
        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
        if !launchedBefore {
            UserDefaults.standard.set(true, forKey: "launchedBefore")
            UserDefaults.standard.synchronize()
            completion()
        }
    }
    
    func renderAnnotations(updateRegion: Bool = false) {
        
        mapkitView.annotations.forEach({ mapkitView.removeAnnotation($0) })
        
        filteredCrimes.forEach { (crime) in
            
            let lat = crime.latitude
            let long = crime.longitude
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D.init(latitude: lat, longitude: long)
            annotation.title = crime.category?.name ?? "???"
            annotation.subtitle = "status: \(crime.status ?? "???")\n" + "adress: \(crime.adress ?? "???")"
            mapkitView.addAnnotation(annotation)
        }

        if updateRegion == true {
            updateRegin(animated: true)
        }
    }
    
    func updateRegin(animated: Bool) {
        let region = MKCoordinateRegionMakeWithDistance(selectedLocation, 700, 700)
        mapkitView.setRegion(region, animated: animated)
    }
    
    func refreshMap() {
        
        mapkitView.annotations.forEach({ mapkitView.removeAnnotation($0) })
        
        selectedLocation = FeedViewController.londonLocation
        
        selectedDate = Date()
        selectedCategory = nil
        
        tableDirector.tableView?.visibleCells.forEach({ (cell) in
            
            if let cell = cell as? ConfigurationCell {
                
                let indexPath = tableDirector.tableView?.indexPath(for: cell)
                
                switch indexPath?.row {
                    case .some(0) : cell.titleLabel.text = FeedViewController.defaultDateKey
                    case .some(1): cell.titleLabel.text = FeedViewController.defaultCategoryKey
                    default :break
                }
            }
        })
        
        mapkitView.removeOverlays(mapkitView.overlays)
        
        let circle = MKCircle(center: selectedLocation, radius: 5)
        self.mapkitView.add(circle)
        
        updateRegin(animated: false)
        
        self.presenter.fetchCrimesCategories(date: selectedDate)
    }

    func renderOptionsSection() {
        
        tableDirector.clear()
        
        let optionsSection = TableSection()
        
        let dateRow = TableRow<ConfigurationCell>.init(item: FeedViewController.defaultDateKey).on(.click) { [unowned self] options in
            
            ActionSheetDatePicker.show(withTitle: "", datePickerMode: .date, selectedDate: self.selectedDate, doneBlock: { (picker, date, _) in
                if let date = date as? Date, let cell = options.cell {
                    self.selectedDate = date
                    self.presenter.fetchCrimesCategories(date: date)
                    cell.titleLabel.text = "Date: " + date.toString(format: "MMM d, yyyy")
                }
            }, cancel: { (picker) in
                return
            }, origin: self.view)
        }
        
        let categoryRow = TableRow<ConfigurationCell>.init(item: FeedViewController.defaultCategoryKey).on(.click) { [unowned self] options in
            
            let index = self.selectedCategory == nil ? 0 : self.crimeCategories.index(of: self.selectedCategory!)
            
            ActionSheetMultipleStringPicker.show(withTitle: "", rows: [self.crimeCategories], initialSelection: [index ?? 0], doneBlock: { picker, index, value in
                
                if let i = index?.first as? Int, let cell = options.cell {
                    self.selectedCategory = self.crimeCategories[i]
                    cell.titleLabel.text = "Category: " + (self.selectedCategory ?? "")
                    self.filterCrimes()
                    self.renderAnnotations()
                }
                
            }, cancel: { (picker) in
                return
            }, origin: self.view)
            
        }
        
        optionsSection.append(rows: [dateRow, categoryRow])
        tableDirector.append(section: optionsSection)
        
        tableDirector.reload()
        
    }
    
    func attemptSearching(searchArea: Double) {
        presenter.fetchCrimes(date: selectedDate, location: selectedLocation, area: searchArea)
    }
    
    func filterCrimes() {
        
        switch selectedCategory {
        case "All crime"?:
            filteredCrimes = originalCrimes
        case nil:
            filteredCrimes = originalCrimes
        default:
            filteredCrimes = originalCrimes.filter { $0.category?.name == selectedCategory }
        }
    }
}

extension FeedViewController:  FeedControllerViewProtocol {
    func crimesUploaded(crimeIds: [Int32]) {
        let crimes: [Crime] = crimeIds.flatMap { Crime.attemptToFetch(withId: Int($0), context: CrimesManager.currentContext) }.filter({ $0.category?.name != nil })
        
        if crimes.isEmpty {
            showAlert(title: "", message: "No crimes for a specific date")
        }
        
        originalCrimes = crimes
        filterCrimes()
        renderAnnotations(updateRegion: true)
    }
    
    func categoriesUploaded(categoryIds: [String]) {
        let categories: [CrimeCategory] = categoryIds.flatMap { CrimeCategory.attemptToFetch(withId: $0, context: CrimesManager.currentContext) }
        crimeCategories = categories.flatMap ({ $0.name })
    }
}

extension FeedViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        if overlay is MKCircle {
            let renderer = MKCircleRenderer(overlay: overlay)
            renderer.strokeColor = .blue
            renderer.fillColor = .blue
            renderer.alpha = 0.5
            return renderer
        }
        return MKOverlayRenderer()
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation { return nil }
        
        let identifier = "marker"
        var view: MKMarkerAnnotationView
        
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            
            let detailLabel = UILabel()
            detailLabel.numberOfLines = 0
            detailLabel.font = detailLabel.font.withSize(12)
            detailLabel.text = annotation.subtitle ?? ""
            view.detailCalloutAccessoryView = detailLabel
        }
        return view
    }
    
}
