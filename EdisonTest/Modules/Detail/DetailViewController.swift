//
//  DetailViewController.swift
//  EdisonTest
//
//  Created by Admin on 11.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

class DetailViewController: UIViewController {

    private var tableDirector: TableDirector!
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableDirector = TableDirector.init(tableView: tableView)
            tableView.tableFooterView = UIView()
        }
    }
    
    var crimes = [Crime]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        renderTableView()
    }
    
    func renderTableView() {
        
        tableDirector.clear()
        
        let format = "d MMM yyyy HH:mm"
        
        var crimesDict = [String: Date]()
        crimes.forEach { crimesDict[($0.timestamp! as Date).toString(format: format) ] = ($0.timestamp! as Date) }
        let sortedTimestamps = Array(crimesDict.values).sorted(by: >)
        
        for date in sortedTimestamps {
            let title = date.toString(format: format)
            let section = TableSection.init(headerTitle: title, footerTitle: "")
            
            let crimes = self.crimes.filter{ title == ($0.timestamp! as Date).toString(format: format) }.sorted(by: { ($0.timestamp! as Date) > ($1.timestamp! as Date) })
            for crime in crimes {
                
                let row = TableRow<CrimeCell>.init(item: (title: crime.category?.name, subtitle: crime.adress, subSubtitle: crime.status))
                section.append(row: row)
            }
            tableDirector.append(section: section)
        }
        

        tableDirector.reload()
    }
}
