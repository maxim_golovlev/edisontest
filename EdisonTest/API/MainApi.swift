//
//  MainApi.swift
//  AteamsTest
//
//  Created by Admin on 20.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

protocol MainApi {
    func sendRequest(withUrl url: String, completion: ServerResult?)
}

enum StatusCode {
    static let ok = 200
    static let badRequest = (key: "Bad Request", value: 400)
    static let serverError = (key: "Internal Server Error", value: 500)
    static let notFoundError = (key: "Error Not Found", value: 404)
    static let unknownError = "Unknown Server Error"
}

extension MainApi {
    
    private func checkStatusCode(responce: URLResponse?) -> String? {
        if let responce = responce as? HTTPURLResponse {
            switch responce.statusCode {
                case StatusCode.ok : return nil
                case StatusCode.serverError.value : return StatusCode.serverError.key
                case StatusCode.badRequest.value : return StatusCode.badRequest.key
                case StatusCode.notFoundError.value : return StatusCode.notFoundError.key
                default : return StatusCode.unknownError
            }
        }
        return "Unknown Response Format"
    }
    
    func sendRequest(withUrl url: String, completion: ServerResult?) {
        
        guard let url = URL.init(string: url)
            else {
                completion?(.error(message: "Incorrect url"))
                return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, responce, error) in
            
            if let statusCodeError = self.checkStatusCode(responce: responce) {
                completion?(.error(message: statusCodeError))
                return
            }
            
            do {
                guard let data = data, let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] else {
                    completion?(.error(message: "error trying to convert data to JSON"))
                    return
                }
                completion?(.success(response: json))
            } catch  {
                completion?(.error(message: "error trying to convert data to JSON"))
                return
            }
            }.resume()
    }
    
    func sendRequest(withUrl url: String, completion: ServerArrayResult?) {
        
        guard let url = URL.init(string: url)
            else {
                completion?(.error(message: "Incorrect url"))
                return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, responce, error) in
            
            if let statusCodeError = self.checkStatusCode(responce: responce) {
                completion?(.error(message: statusCodeError))
                return
            }
            
            do {
                guard let data = data, let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: AnyObject]] else {
                    completion?(.error(message: "error trying to convert data to JSON"))
                    return
                }
                completion?(.success(response: json))
            } catch  {
                completion?(.error(message: "error trying to convert data to JSON"))
                return
            }
            }.resume()
    }
    
}
