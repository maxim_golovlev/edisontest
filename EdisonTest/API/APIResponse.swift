import Foundation

enum APIResponse {
    case success (response: Dictionary<String, AnyObject>)
    case error (message: String?)
}

typealias ServerResult = (_ response: APIResponse) -> Void

enum APIArrayResponse {
    case success (response: [Dictionary<String, AnyObject>])
    case error (message: String?)
}

typealias ServerArrayResult = (_ response: APIArrayResponse) -> Void

enum ResponseError: Error {
    case withMessage(String?)
}
