//
//  Extensions.swift
//  EdisonTest
//
//  Created by Admin on 10.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreData

extension Date {
    func toString(format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}

extension NSManagedObject {
    
    class func attemptToSave(context: NSManagedObjectContext = CrimesManager.currentContext) {
        
      //  context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        if context.hasChanges {
            do {
                try context.save()
            } catch let error {
                print("coredata saving error\(error.localizedDescription)")
            }
        }
    }
    
    class func createOrReturn<T: NSManagedObject>(withId id: String?, context: NSManagedObjectContext = CrimesManager.currentContext) -> T {
        
        var objectToReturn: T
        let entityName = String(describing: T.self)
        
        if let oldObject: T = attemptToFetch(withId: id, context: context) {
            objectToReturn = oldObject
            
        } else {
            let newObject = NSEntityDescription.insertNewObject(forEntityName: entityName, into: context) as! T
            objectToReturn = newObject
        }
        
        NSManagedObject.attemptToSave(context: context)
        
        return objectToReturn
    }
    
    
    class func attemptToFetch<T: NSManagedObject>(withId id: String?, context: NSManagedObjectContext) -> T? {
        
        guard let id = id else { return nil }
        
        let object: T? = fetch(predicate: NSPredicate(format: "id = %@", id), context: context)?.first
        return object
    }
    
    class func createOrReturn<T: NSManagedObject>(withId id: Int?, context: NSManagedObjectContext = CrimesManager.currentContext) -> T {
        
        var objectToReturn: T
        let entityName = String(describing: T.self)
        
        if let oldObject: T = attemptToFetch(withId: id, context: context) {
            objectToReturn = oldObject
            
        } else {
            let newObject = NSEntityDescription.insertNewObject(forEntityName: entityName, into: context) as! T
            objectToReturn = newObject
        }
        
        NSManagedObject.attemptToSave(context: context)
        
        return objectToReturn
    }
    
    
    class func attemptToFetch<T: NSManagedObject>(withId id: Int?, context: NSManagedObjectContext) -> T? {
        
        guard let id = id else { return nil }
        
        let object: T? = fetch(predicate: NSPredicate(format: "id = %d", id), context: context)?.first
        return object
    }
    
    class func fetch<T: NSManagedObject>(predicate: NSPredicate? = nil, sortDescriptors: [NSSortDescriptor] = [], context: NSManagedObjectContext) -> [T]? {
        
        var objects:[T]?
        
        do {
            let entityName = String(describing: T.self)
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
            fetchRequest.returnsObjectsAsFaults = false
            fetchRequest.predicate = predicate
            fetchRequest.sortDescriptors = sortDescriptors
            objects = try context.fetch(fetchRequest) as? [T]
            return objects
            
        } catch let error {
            print(error)
        }
        
        return nil
    }
}

