//
//  ConfigurationCellTableViewCell.swift
//  EdisonTest
//
//  Created by Admin on 10.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

class ConfigurationCell: UITableViewCell, ConfigurableCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    static var defaultHeight: CGFloat? {
        return 44
    }
    
    func configure(with title: String?) {
        titleLabel.text = title
    }
}
