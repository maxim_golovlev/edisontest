//
//  CrimeCell.swift
//  EdisonTest
//
//  Created by Admin on 11.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

class CrimeCell: UITableViewCell, ConfigurableCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var subSubtitleLabel: UILabel!
    
    static var defaultHeight: CGFloat? {
        return 80
    }

    func configure(with data:(title: String?, subtitle: String?, subSubtitle: String?)) {
        titleLabel.text = data.title ?? "???"
        subtitleLabel.text = data.subtitle ?? "???"
        subSubtitleLabel.text = data.subSubtitle ?? "???"
    }
    
}
