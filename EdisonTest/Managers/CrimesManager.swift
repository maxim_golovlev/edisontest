//
//  CrimesManager.swift
//  EdisonTest
//
//  Created by Admin on 10.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import MapKit
import PromiseKit
import CoreData

class CrimesManager: NSObject {
    
    private override init() { }
    
    static let shared = CrimesManager()
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    static var currentContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    static var appPersistentContainer = (UIApplication.shared.delegate as! AppDelegate).persistentContainer
    
    struct APIKeys {
        static func locAtPoint(date: Date, location: CLLocationCoordinate2D) -> String {
            let lat = location.latitude
            let long = location.longitude
            let dateString = date.toString(format: "YYYY-MM")
            return "https://data.police.uk/api/crimes-street/all-crime?lat=\(lat)&lng=\(long)&date=\(dateString)"
        }
        
        static func locAtCustomArea(date: Date, location: CLLocationCoordinate2D, area: Double) -> String {
            let lat = location.latitude
            let long = location.longitude
            let dateString = date.toString(format: "YYYY-MM")
            let square = "\(lat + area),\(long + area):\(lat + area),\(long - area):\(lat - area),\(long - area):\(lat - area),\(long + area)"
            return "https://data.police.uk/api/crimes-street/all-crime?poly=\(square)&date=\(dateString)"
        }
        
        static func crimeCategories(date: Date) -> String {
            let dateString = date.toString(format: "YYYY-MM")
            return "https://data.police.uk/api/crime-categories?date=\(dateString)"
        }
    }
    
    func fetchCrimes(date: Date, location: CLLocationCoordinate2D, area: Double) -> Promise<[Int32]> {
        
        let urlString = APIKeys.locAtCustomArea(date: date, location: location, area: area)
        
        return Promise.init(resolvers: { (fullfill, reject) in
            
            APIManager.shared.fetchItems(withUrl: urlString, completion: { response in
                
                switch response {
                case let .success(response: json):
                    
                    CrimesManager.appPersistentContainer.performBackgroundTask({ (context) in
                        let crimes = json.flatMap { Crime.create(dict: $0, context: context) }
                        let crimesId = crimes.flatMap { $0.id }
                        fullfill(crimesId)
                    })

                case let .error(message: msg):
                    reject(ResponseError.withMessage(msg))
                }
            })
        })
    }
    
    func fetchCrimeCategories(date: Date) -> Promise<([String])> {
        
        let urlString = APIKeys.crimeCategories(date: date)
        
        return Promise.init(resolvers: { (fullfill, reject) in
            
            APIManager.shared.fetchItems(withUrl: urlString, completion: { response in
                
                switch response {
                case let .success(response: json):

                    CrimesManager.appPersistentContainer.performBackgroundTask({ (context) in
                        
                        let categories = json.flatMap { CrimeCategory.create(dict: $0, id: nil, context: context)}
                        
                        let categoriesTd = categories.flatMap { $0.id }
                        
                        fullfill(categoriesTd)
                    })

                case let .error(message: msg):
                    reject(ResponseError.withMessage(msg))
                }
            })
        })
    }
}
