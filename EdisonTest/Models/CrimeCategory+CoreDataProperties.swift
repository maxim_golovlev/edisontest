//
//  CrimeCategory+CoreDataProperties.swift
//  EdisonTest
//
//  Created by Admin on 10.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//
//

import Foundation
import CoreData


extension CrimeCategory {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CrimeCategory> {
        return NSFetchRequest<CrimeCategory>(entityName: "CrimeCategory")
    }

    @NSManaged public var name: String?
    @NSManaged public var id: String?

}
