//
//  CrimeCategory+CoreDataClass.swift
//  EdisonTest
//
//  Created by Admin on 10.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//
//

import Foundation
import CoreData

public class CrimeCategory: NSManagedObject {
    
    static func create(dict: [String: Any]?, id: String?, context: NSManagedObjectContext) -> CrimeCategory? {
        
        var _id = id
        
        if let dict = dict, let idd = dict["url"] as? String {
            _id = idd
        }
        
        let category: CrimeCategory = CrimeCategory.createOrReturn(withId: _id, context: context)
        category.initfromDict(dict: dict, context: context)
        
        return category
    }
    
    static func id(dict: [String: Any]?) -> String? {
        guard let dict = dict else { return nil }
        return dict["url"] as? String
    }

    private func initfromDict(dict: [String: Any]?, context: NSManagedObjectContext) {
        guard let dict = dict, let name = dict["name"] as? String, let id = dict["url"] as? String else { return }
        self.name = name
        self.id = id
        
        NSManagedObject.attemptToSave(context: context)
    }
}
