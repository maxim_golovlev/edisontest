//
//  Crime+CoreDataProperties.swift
//  EdisonTest
//
//  Created by Admin on 11.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//
//

import Foundation
import CoreData


extension Crime {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Crime> {
        return NSFetchRequest<Crime>(entityName: "Crime")
    }

    @NSManaged public var adress: String?
    @NSManaged public var id: Int32
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var status: String?
    @NSManaged public var timestamp: NSDate?
    @NSManaged public var category: CrimeCategory?

}
