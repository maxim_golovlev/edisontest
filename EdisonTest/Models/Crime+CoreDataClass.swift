//
//  Crime+CoreDataClass.swift
//  EdisonTest
//
//  Created by Admin on 10.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//
//

import Foundation
import CoreData

public class Crime: NSManagedObject {
    
    static func create(dict: [String: Any], context: NSManagedObjectContext) -> Crime {
        
        let crime: Crime = Crime.createOrReturn(withId: dict["id"] as? Int, context: context)
        crime.initfromDict(dict: dict, context: context)
        
        return crime
    }

    private func initfromDict(dict: [String: Any], context: NSManagedObjectContext) {
        
        if self.timestamp == nil {
            self.timestamp = Date() as NSDate
        }
        
        if let categoryId = dict["category"] as? String {
            self.category = CrimeCategory.create(dict: nil, id: categoryId, context: context)
        }
        
        if let id = dict["id"] as? Int32 {
            self.id = id
        }
        
        if let location = dict["location"] as? [String: Any],
            let lat = location["latitude"] as? String,
            let long = location["longitude"] as? String,
            let street = location["street"] as? [String: Any],
            let streetId = street["id"] as? Int,
            let streetName = street["name"] as? String {
            
            if let lat = Double(lat), let long = Double(long) {
                self.longitude = long
                self.latitude = lat
            }
            self.adress = "\(streetName), \(streetId)"
        }
        
        if let status = dict["outcome_status"] as? [String: Any],
            let statusCategory = status["category"] as? String {
            self.status = "\(statusCategory)"
        }
        return NSManagedObject.attemptToSave(context: context)
    }
}
